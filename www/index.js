import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import AuthPlugin from "./plugins/auth";
import VueApollo from "vue-apollo";
import ApolloClient from 'apollo-boost'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'



const apolloClient = new ApolloClient({
  // You should use an absolute URL here
  uri: 'https://localhost:1234'
})

console.log();
// HTTP connection to the APIs
const httpLink = createHttpLink({
  // You should use an absolute URL here
  //uri: 'http://localhost:3020/graphql',
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
})

const apolloProvider = new VueApollo({
   defaultClient: client,
 })

new Vue({
    router,
    apolloProvider,
    render: h => h(App)
  }).$mount("#app");
