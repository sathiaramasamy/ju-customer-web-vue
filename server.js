const express = require("express");
const cors = require("cors");
const os = require("os");
const rp = require("request-promise-native");
const internalIp = require("internal-ip");
const fs = require("fs");
const zipdir = require("zip-dir");

const app = express();
app.use(cors());
app.use(express.static("www"));
app.listen(process.env.PORT || 1234);

console.log("Running on port 8888");

// The code below is used to provide real-time updates to the cordova app while debugging

var lastUpdated = Date.now();

// Watches the www folder for changes, and updates the lastUpdated time stamp when a file is changed
fs.watch('./www', {recursive:true},function(eventType, filename){
  lastUpdated = Date.now();
});

// Used by the cordova app to get the latest version identifier
app.get("/update/latest.json",function(req,res){
  res.json(lastUpdated + ".zip");
});

// Used by the cordova app to get the latest version source code
app.get("/update/*.zip",function(req,res){
  // Zips the www folder
  zipdir("./www", function (err, buffer) {
    if(err){
      console.log("Zip Failed",err);
    }
    else{
      // Delivers the zip file
      res.writeHead(200, {'Content-Type': 'application/zip'});
      res.end(buffer, 'binary');
    }
  });
});

// Pings our debug endpoint to make it easier for the app to discover local developer machines
function ping(){
  rp.post("https://api.gokaizen.com/v1/common/devmachines",{
    body:{
      hostname:os.hostname(),
      localip:internalIp.v4.sync(),
      app:"customer"
    },
    json:true
  });
}

// Runs the ping function on startup, then once a minute
setInterval(ping,60*1000);
ping();
